package net.progress.selenium.desi_exam.registerAndLogin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.And;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utils.Common;
import contexts.Context;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.LoginPF;

public class BDDRegisterAndLogin {
    private LoginPF loginPage;
    private Context context;
    WebDriver driver;

    public BDDRegisterAndLogin(Context context) {
        this.context = context;
        driver = Common.getDriver();
        loginPage = new LoginPF(driver);
    }

    @Given("There is user with valid email")
    public void validEmail() {
        loginPage.load();
        loginPage.isLoaded();
    }

    @Given("The user click sign in button and enter email")
    public void makingARegistration() {
        loginPage.signInButtonClick();
        loginPage.validEmail();
    }

    @When("The user press the create an account button")
    public void pressRegisterButton() {
        loginPage.createAnAccountButtonPress();
    }

    @Then("The user is successfully redirected to Register page")
    public void successfullyRegistered() throws InterruptedException {
        assertEquals("Redirect is not successful", "YOUR PERSONAL INFORMATION", loginPage.isRedirectSuccessful());

        driver.quit();
    }

    @Given("There is user with invalid email")
    public void invalidEmail() {
        loginPage.load();
        loginPage.isLoaded();
    }

    @Given("The user enters an invalid (.*) and (.*) and clicks Log In")
    public void InvalidALogIn(String email, String password) {
        loginPage.signInButtonClick();
        loginPage.invalidLogIn(email, password);
    }

    @Then("Error message shows")
    public void errorMessageShows() {
        assertEquals("Error message not the expected one", "Invalid email address.", loginPage.emailError());

        driver.quit();
    }

    @Given("The user is on the home page and clicks on the Sign in button")
    public void clickOnSignIn() {
        loginPage.load();
        loginPage.isLoaded();
        loginPage.clickSignIn();
    }

    @And("The user enters a valid email address and clicks Create account")
    public void signInPage() {
        loginPage.createAccount();
        context.emailAddress = loginPage.getUsername();

    }

    @Given("The user enters their(.*), (.*), (.*), (.*), (.*), (.*), and (.*)")
    public void fillUserParameters(String firstName, String lastName, String password, String address,
                                   String city, String zipcode, String phone) {
        context.password = password;
        loginPage.enterUserInfo(firstName, lastName, password, address,
                city, zipcode, phone);

    }

    @When("The user clicks on Register button")
    public void registerClick() {
        loginPage.clickRegister();
    }

    @Then("The user is redirected to My account page")
    public void redirectCheck() {
        assertEquals("Missing Welcome text", "Welcome to your account. Here you can manage "
                + "all of your personal information and orders.", loginPage.welcomeTextAssert());

    }


    @Given("There is existing user with valid email")
    public void existingUser() {
        loginPage.load();
        loginPage.signInButtonClick();
    }

    @When("The user fill the fields and click sign in button")
    public void formFilling() {
        loginPage.login(context.password);
    }

    @And("The user enters an invalid email address and clicks Create account")
    public void theUserEntersAnInvalidEmailAddressAndClicksCreateAccount() {
        loginPage.invalidAccountAttempt();
    }

    @Then("An Invalid Email error message appears")
    public void anErrorMessageAppears() {
//		boolean exists =loginPage.errorExists();
//		assertTrue("No error message", exists);

        String error = loginPage.emailError();
        assertEquals("Invalid email is accepted.", error, "Invalid email address.");
        driver.quit();
    }


    @Then("The user receives an error for missing First and Last name")
    public void theUserReceivesAnErrorForMissingFirstName() {
        assertTrue("Incorrect error message", loginPage.errorFirstandLastName());
    }

    @And("The user submits their (.*), (.*), (.*), (.*), and (.*)")
    public void theUserSubmitsTheirPasswordAddressCityZIPCodeAndMobilePhone(String password, String address,
                                                                            String city, String zipcode, String phone) {
        loginPage.missingInfo(password, address, city, zipcode, phone);
    }

    @Then("the user clicks on Sign Out")
    public void theUserClicksOnSignOut() {
        loginPage.signOutButton();

    }

    @And("The user clicks on Sign In and enters their username and password")
    public void theUserClicksOnSignInAndEntersTheirUsernameAndPassword() {
        loginPage.signInButtonClick();
        loginPage.logInInfo(context.emailAddress, context.password);
        loginPage.clickLogIn();
    }

    @Then("The user receives an error")
    public void theUserReceivesAnError() {
        String error = loginPage.invalidEmailError();
        assertTrue(error.contains("error"));
    }
}
