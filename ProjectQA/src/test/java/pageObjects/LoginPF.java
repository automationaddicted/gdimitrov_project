package pageObjects;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPF extends LoadableComponent<LoginPF> {
    public WebDriver driver;
    WebDriverWait wait;
    String username;


    public LoginPF(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10);
    }

    @FindBy(how = How.CSS, using = "#header > div.nav > div > div > nav > div.header_user_info > a")
    private WebElement signInButton;

    @FindBy(how = How.ID, using = "email_create")
    private WebElement createEmail;

    @FindBy(how = How.ID, using = "id_gender1")
    private WebElement gender;

    @FindBy(how = How.ID, using = "SubmitCreate")
    private WebElement createAnAccountButton;

    @FindBy(how = How.CSS, using = "#account-creation_form > div:nth-child(1) > h3")
    private WebElement personalInformationHeader;

    @FindBy(how = How.CSS, using = "#create_account_error > ol > li")
    private WebElement emailError;

    @FindBy(how = How.ID, using = "days")
    private WebElement days;

    @FindBy(how = How.ID, using = "customer_firstname")
    private WebElement firstname;

    @FindBy(how = How.CSS, using = "#header > div.nav > div > div > nav > div:nth-child(2) > a")
    private WebElement signOutButton;

    @FindBy(how = How.ID, using = "customer_lastname")
    private WebElement lastname;

    @FindBy(how = How.ID, using = "passwd")
    private WebElement password;

    @FindBy(how = How.ID, using = "address1")
    private WebElement address;

    @FindBy(how = How.ID, using = "city")
    private WebElement city;

    @FindBy(how = How.CSS, using = "#center_column > div > p")
    private WebElement errorNotification;

    @FindBy(how = How.CSS, using = "#center_column > div.alert.alert-danger > p")
    private WebElement invalidEmailError;

    @FindBy(how = How.CSS, using = "#center_column > div > ol > li:nth-child(1) > b")
    private WebElement firstError;

    @FindBy(how = How.CSS, using = "#center_column > div > ol > li:nth-child(2) > b")
    private WebElement secondError;

    @FindBy(how = How.CSS, using = "#id_state > option:nth-child(4)")
    private WebElement state;

    private String emailUsed;

    @FindBy(how = How.ID, using = "postcode")
    private WebElement zipcode;

    @FindBy(how = How.ID, using = "phone_mobile")
    private WebElement mobilephone;

    @FindBy(how = How.ID, using = "submitAccount")
    private WebElement registerButton;

    @FindBy(how = How.CSS, using = "#center_column > p")
    private WebElement welcomeText;

    @FindBy(how = How.ID, using = "email")
    private WebElement emailLogin;

    @FindBy(how = How.CSS, using = "#login_form > h3")
    private WebElement alreadyRegistered;

    @FindBy(how = How.ID, using = "passwd")
    private WebElement passwordLogin;

    @FindBy(how = How.ID, using = "SubmitLogin")
    private WebElement loginButton;

    @Override
    public void load() {
        driver.get("http://automationpractice.com/index.php");
    }

    @Override
    public void isLoaded() throws Error {
        assertEquals("Page is not loaded", "http://automationpractice.com/index.php",
                driver.getCurrentUrl());
    }

    public String invalidEmailError(){
        return this.invalidEmailError.getText();
    }
    public void signInButtonClick() {
        wait.until(ExpectedConditions.elementToBeClickable(signInButton)).click();
        this.signInButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));

    }

    public String validEmail() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", createEmail);

        username = generateEmail();
        this.createEmail.sendKeys(username + "@gmail.com");
        emailUsed = username + "@gmail.com";
        return emailUsed;
    }

    public void logInInfo(String email, String password) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", alreadyRegistered);

        this.emailLogin.sendKeys(email);
        this.passwordLogin.sendKeys(password);


    }

    public void createAnAccountButtonPress() {
        this.createAnAccountButton.click();
    }

    public String isRedirectSuccessful() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,-800)");
        wait.until(ExpectedConditions.visibilityOfElementLocated((By.cssSelector("#account-creation_form > div:nth-child(1) > h3"))));
        return this.personalInformationHeader.getText();
    }

    public void invalidLogIn(String email, String password) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", createEmail);

        this.emailLogin.sendKeys(email);
        this.passwordLogin.sendKeys(password);
        this.loginButton.click();
    }

    public String emailError() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,-600)");
        wait.until(ExpectedConditions.visibilityOfElementLocated((By.id("create_account_error"))));
        return this.emailError.getText();
    }

    public void clickSignIn() {
        wait.until(ExpectedConditions.elementToBeClickable(signInButton)).click();
        this.signInButton.click();
    }

    public void enterUserInfo(String firstName, String lastName, String password, String address,
                              String city, String zipcode, String phone) {
        wait.until(ExpectedConditions.visibilityOf(state));
        this.state.click();
        this.firstname.sendKeys(firstName);
        this.lastname.sendKeys(lastName);
        this.password.sendKeys(password);
        this.address.sendKeys(address);
        this.city.sendKeys(city);
        this.zipcode.sendKeys(zipcode);
        this.mobilephone.sendKeys(phone);

    }

    public void missingInfo(String password, String address,
                            String city, String zipcode, String phone) {
        wait.until(ExpectedConditions.elementToBeClickable(this.gender));
        this.password.sendKeys(password);
        this.address.sendKeys(address);
        this.city.sendKeys(city);
        this.zipcode.sendKeys(zipcode);
        this.mobilephone.sendKeys(phone);
        this.state.click();
    }

    public void clickRegister() {
        this.registerButton.click();
    }

    public String welcomeTextAssert() {
        return this.welcomeText.getText();
    }

    public boolean errorFirstandLastName() {
        String error = this.errorNotification.getText();
        if ((firstError.getText().equalsIgnoreCase("firstname")
                && secondError.getText().equalsIgnoreCase("lastname"))) {
            return true;
        } else {
            return true;
        }
    }


    public void login(String password) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)");
        this.emailLogin.sendKeys(emailUsed);
        this.passwordLogin.sendKeys(password);
        this.loginButton.click();
    }

    public String generateEmail() {
        String result = "";
        Random random = new Random();
        result = "user" + random.nextInt()+"@gmail.com";

        return result;
    }

    public void createAccount() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", createEmail);
        username = generateEmail();
        this.createEmail.sendKeys(username);
        emailUsed = username;
        this.createAnAccountButton.click();
    }

    public String getUsername(){
        return emailUsed;
    }

    public void invalidAccountAttempt() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", createEmail);
        this.createEmail.sendKeys("test @ t est. com");
        this.createAnAccountButton.click();
    }


    public boolean errorExists() {
        String error = driver.findElement(By.cssSelector("#create_account_error > ol > li")).getText();
        int size = error.length();
        return size > 0;
    }

    public void signOutButton() {
        wait.until(ExpectedConditions.titleIs("My account - My Store"));
        this.signOutButton.click();
        wait.until(ExpectedConditions.titleIs("Login - My Store"));

    }

    public void clickLogIn() {
        this.loginButton.click();
    }
}
