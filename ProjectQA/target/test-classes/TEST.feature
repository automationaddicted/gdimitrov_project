Feature: Basic logic for login and registration

  Scenario Outline: Successful registration
    Given The user is on the home page and clicks on the Sign in button
    And The user enters a valid email address and clicks Create account
    And The user enters their <First name>, <Last name>, <Password>, <Address>, <City>, <ZIP Code>, and <Mobile phone>
    When The user clicks on Register button
    Then The user is redirected to My account page
    Examples:
      | First name | Last name | Password  | Address         | City  | ZIP Code | Mobile phone |
      | Test       | Test      | Pa$$word! | Bulgaria Street | Sofia | 48573    | 4896653      |
      | John       | Lennon    | Pa$$word! | Vitosha Street  | Vidin | 34235    | 4896653      |

  Scenario: Registration attempt with invalid email
    Given The user is on the home page and clicks on the Sign in button
    And The user enters an invalid email address and clicks Create account
    Then An Invalid Email error message appears

  Scenario Outline: Unsuccessful registration due to missing required field
    Given The user is on the home page and clicks on the Sign in button
    And The user enters a valid email address and clicks Create account
    And The user submits their <Password>, <Address>, <City>, <ZIP Code>, and <Mobile phone>
    When The user clicks on Register button
    Then The user receives an error for missing First and Last name
    Examples:
      | Password     | Address         | City  | ZIP Code | Mobile phone |
      | Pa$$word!    | Bulgaria Street | Sofia | 48573    | 4896423653   |
      | P34454fword! | Vitosha Street  | Varna | 48573    | 4896653      |

  Scenario Outline: Login Successful
    Given The user is on the home page and clicks on the Sign in button
    And The user enters a valid email address and clicks Create account
    And The user enters their <First name>, <Last name>, <Password>, <Address>, <City>, <ZIP Code>, and <Mobile phone>
    When The user clicks on Register button
    And The user is redirected to My account page
    Then the user clicks on Sign Out
    And The user clicks on Sign In and enters their username and password
    Then The user is redirected to My account page
    Examples:
      | First name | Last name | Password  | Address         | City  | ZIP Code | Mobile phone |
	  | Test       | Test      | Pa$$word! | Bulgaria Street | Sofia | 43073    | 4896653      |

	Scenario Outline: Login Unsuccessful
		Given The user is on the home page and clicks on the Sign in button
		And The user enters an invalid <Email> and <Password> and clicks Log In
		Then The user receives an error
		Examples:
			| Email          | Password  |
			| Test . com     | Pa$$word! |
			| email . com     | Pa$$word! |